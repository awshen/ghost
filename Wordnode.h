/* 
 * File:   Wordnode.h
 * Author: awshen
 *
 * Created on November 5, 2015, 2:08 PM
 */

#ifndef WORDNODE_H
#define	WORDNODE_H

#define PTR_ARRAY_SIZE 26

#include <vector>
#include <string>
using namespace std;

class Wordnode {

friend class Wordgraph;

public:
    
    ~Wordnode();

    vector<Wordnode *> getChildren();
    
    void printString();
    
    string getString();
    
    bool getIsLeaf();

    Wordnode * getChildNode(char c);
    
    void setChildNode(Wordnode * node, char c);

    void walk();  
    
  //polls a node and returns 0 if it is losing, 1 if it is winning, and also tags the node as such.
    bool tagWinLose();
    
    //Given a string s, returns the next char needed to get to s
    //returns ' ' if the child word node is not accessible from s
    char nextChar(string s);
    
    //return vector of next possible moves from this node
    vector<string> nextMoves();

protected:
    
    Wordnode(string s, bool b);
  
private: 
    
    string word_string;
    int color = -1; //1 indicates odd-numbered move (white) and 0 indicates even numbered move (black)
    bool type = false; //type of node: losing (0), or winning (1)
    bool is_leaf;
    Wordnode * ptr_array[PTR_ARRAY_SIZE];

    //helper fxn for nextMoves - applied to losing nodes only
    vector <string> longestWords();
    
};


#endif	/* WORDNODE_H */

