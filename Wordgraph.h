/* 
 * File:   Wordgraph.h
 * Author: aws9a
 *
 * Created on November 4, 2015, 8:17 PM
 */

#ifndef WORDGRAPH_H
#define	WORDGRAPH_H

#include <string>
using namespace std;

#include "Wordnode.h"

class Wordgraph {

private:
    Wordnode * rootWordnode;
    const int min_length = 4;
    Wordgraph(Wordgraph const &);
    void operator=(Wordgraph const &);

public:    
    Wordgraph();
    
    ~Wordgraph();
    
    void tagAll();
    
    void traverseWordGraph();
    
    Wordnode * findWordnode(string s);
    
    //returns a random word accessible from wordnode w
    string rndWordInBranch(Wordnode * w);
    
    //insInWordGraph: string -> bool
    //attempts to insert the string into the WordGraph.  If successful, return true.
    //if word has the same beginning as an existing word, return false and do not insert the word.
    //fails if:
    //1) s consists of (another word already in the graph + 0 or more letters)
    //2) s is too short
    bool insInWordGraph(string s);
    
};


#endif	/* WORDGRAPH_H */

