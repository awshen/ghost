/* 
 * Author: Aaron Shen
 *  
 * (c) 2015
 *    
*/

#include "Wordnode.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;

Wordnode::Wordnode(string s, bool b){
     word_string = s;
     is_leaf = b;
     color = s.length()%2;
     memset(ptr_array,0,sizeof(Wordnode * ) * PTR_ARRAY_SIZE);
}    
 

 Wordnode::~Wordnode(){
        vector<Wordnode *> tmp = getChildren();
        for (int i=0; i<tmp.size(); i++){
            delete(tmp[i]);
        }
    }

vector<Wordnode *> Wordnode::getChildren(){
   vector<Wordnode *> children;
    for (int i=0; i<PTR_ARRAY_SIZE;i++){
        if (this->ptr_array[i]){                
            children.push_back(this->ptr_array[i]);
        }
    }
   return children;
}

void Wordnode::printString() {
    cout << word_string << endl;
    cout << "Is Leaf: " << is_leaf << endl;
    cout << "Dest: " << type << endl; 
    cout << "Color: " <<  color << endl; 
}

string Wordnode::getString() {
    return word_string;
}

bool Wordnode::getIsLeaf(){
    return is_leaf;
}

Wordnode * Wordnode::getChildNode(char c) {
    return this->ptr_array[c -'a'];	  
}

void Wordnode::setChildNode(Wordnode * node, char c) {
    ptr_array[c - 'a'] = node;
}

void Wordnode::walk() {
    if (is_leaf) {
        printString();
    }
    else {
        printString();
        for (int i=0;i<PTR_ARRAY_SIZE;i++){
            if (ptr_array[i]) ptr_array[i]->walk();              
        }
    }      
}    

//polls a node and returns 0 if it is losing, 1 if it is winning, and also tags the node as such.
bool Wordnode::tagWinLose() {
    //case 1: no children (leaf node)
    if (is_leaf) {
        if (color==1) {//white lost, black wins
          type = true;
          return type;
        }
        else {//black lost, white wins
          type = false;
          return type;
        }
    }

    vector<Wordnode *> children = this->getChildren();     

    //case 2: one child
    if (children.size()==1) {
        type = children[0]->tagWinLose();
        return type;
    }
    //case 3: 2+ children
    else {
        int len = children.size();
        if (color==0) {
            type = true; //initially set type = true
            for (int i=0;i<len;i++){
                if (children.at(i)->tagWinLose() == false) {
                    type = false;  //return false if any of the children is losing
                }
            }
            return type; //all children are winning, return true
        }
        else { //color == 1
            type = false; //initially set type = false
            for (int i=0;i<len;i++){
                if (children.at(i)->tagWinLose() == true) {
                    type = true;  //return true if any of the children is winning
                }
            }
            return type; //all children are losing, return false
        } 
    }
}

//Given a string s, returns the next char needed to get to s
//returns ' ' if the child word node is not accessible from s
char Wordnode::nextChar(string s) {
    //System.out.println(word_string + " " + s);
    int my_length = word_string.length();
    //System.out.println(l);
    if (my_length >= s.length()) { //ensures s is strictly longer than l
        return ' '; //error
    }
    if (strncmp(word_string.c_str(),s.c_str(),my_length) != 0 )  { //word_string must be entirely contained within s
        return ' '; //error
    }
    return s.at(my_length);

}

//return vector of next possible moves from this node
vector<string> Wordnode::nextMoves() {
    vector<string> result;
    if (this->type) { //node is winning, choose between all winning child nodes
        for (int i=0; i < PTR_ARRAY_SIZE; i++){
            if (ptr_array[i] && ptr_array[i]->type) {
                result.push_back(ptr_array[i]->word_string);
            }
        }          
        return result;
    }   
    else { //node is losing, attempt to prolong match as long as possible
        vector<string> longest = this->longestWords();
        result.insert(result.end(),longest.begin(),longest.end() );          
        return result;
    }      

}

//helper fxn for nextMoves - applied to losing nodes only
vector <string> Wordnode::longestWords() {
    vector<string> result;
    //case 1: no children (leaf node)
    if (is_leaf) {
        result.push_back(word_string);
        return result;
    }
    //case 2: one child
    vector<Wordnode *> children = this->getChildren();
    if (children.size()==1) {
        vector<string> tmp = children[0]->longestWords();
        result.insert(result.end(),tmp.begin(),tmp.end());
        return result;
    }
    //case 3: 2+ children
    else {
        if (color==0) { //assume white will make (winning) choice that will end the game as quickly as possible
            for (int i=0;i < children.size();i++){
                if (children[i]->type == false) { //examine losing branches only
                    if (result.size() == 0) { //node is first losing node we've seen
                     vector<string> tmp = children[i]->longestWords();
                      result.insert(result.end(), tmp.begin(), tmp.end());
                    }
                    else { //compare current node with existing result
                        vector<string> cmp = children[i]->longestWords();
                        if (cmp[0].length() < result[0].length()) {//word length from this node is shortest so far
                            result.clear();
                            result.insert(result.end(),cmp.begin(),cmp.end());
                        }
                        else if (cmp[0].length() == result[0].length()) {  //word length from this node is equal to shortest so far
                            result.insert(result.end(),cmp.begin(),cmp.end());
                        }
                        else { //word length from this node is higher than what we've seen so far, don't consider this path
                        }          
                    }
                }

            }
          return result;
        }
        else { //black can make choice that will extend the game as long as possible (we assume all child nodes are losing)
            for (int i=0;i < children.size();i++){
                if (result.size() == 0) { //node is first losing node we've seen
                    vector<string> tmp = children[i]->longestWords();                    
                    result.insert(result.end(), tmp.begin(), tmp.end());
                }
                else { //compare current node with existing result
                    vector<string> cmp = children[i]->longestWords();
                    if (cmp[0].length() > result[0].length()) {//word length from this node is longest so far
                        result.clear();
                        result.insert(result.end(), cmp.begin(), cmp.end());
                    }
                    else if (cmp[0].length() == result[0].length()) {  //word length from this node is equal to longest so far
                        result.insert(result.end(),cmp.begin(),cmp.end());
                    }
                    else { //word length from this node is lower than what we've seen so far, don't consider this path
                    }
                }   
            }
            return result;
        }
    }
}
