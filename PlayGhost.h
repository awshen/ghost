/*
 * 
 * Functions to dictate progress of the game
 * 
 * Author: Aaron Shen
 * 
 * (c) 2015
 */

#ifndef PLAYGHOST_H
#define	PLAYGHOST_H

#include <cstdlib>
#include <fstream>
#include <cctype>
#include <iostream>
using namespace std;

#include "Wordgraph.h"

/**
 *
 * @author aaron
 */
class PlayGhost {
    
private:
    Wordgraph * g;
    bool in_progress;
    static bool initialized;
    
    PlayGhost(string filename);
    
    PlayGhost(PlayGhost const &);
    void operator=(PlayGhost const &);
    
    //process a human move
	//if game over, send a game over msg to the client
    //if game not over, send the next word to the client
    string processMove(string current_string, char next_letter);    

    char compMove(Wordnode * currentnode);
    
    void printGameOverMsg(int condition, Wordnode * currentnode);      
    
public:
    //PlayGhost(String word_list_path) 
    
    static PlayGhost * initPlayGhost(string filename);
    
    ~PlayGhost();
    
    void startGame();
    
};

#endif

