/* 
 * Author: Aaron Shen
 *  
 * (c) 2015
 *    
*/
#include "PlayGhost.h"
bool PlayGhost::initialized = false;

 PlayGhost::PlayGhost(string filename) {
        in_progress = false;
        g = new Wordgraph();
        ifstream in(filename, ios_base::in);
        if (!in.is_open()){
            cout << "Couldn't open the file words.txt.  Please make sure this file exists in the same directory." << endl;
            exit(1);
        }
        
        //read in dictionary        
        string curr_line;
        while (getline(in,curr_line)){
            g->insInWordGraph(curr_line);
        }
        in.close();
        
        //g->traverseWordGraph();
        
        g->tagAll();
    }
 
PlayGhost * PlayGhost::initPlayGhost(string filename) {
    if (!initialized) {
        initialized = true;
        return new PlayGhost(filename);
    }
}


PlayGhost::~PlayGhost(){
    delete(g);
}

void PlayGhost::startGame(){
    in_progress = true;
    cout << "Game started.  You go first." << endl;        
    string current_string="";
    while(in_progress){
        char c;
        cout << "Enter a letter: ";
        cin >> c;                    
        while (!isupper(c) && !islower(c)) {
            cout << "Enter a letter: ";
            cin >> c;
        }
        c = tolower(c);

        current_string = processMove(current_string,c);
    }
}

//process a human move
    //if game over, send a game over msg to the client
//if game not over, send the next word to the client
string PlayGhost::processMove(string current_string, char next_letter){

    Wordnode * currentnode;

    currentnode = g->findWordnode(current_string);

    //check for legal human move
    if (currentnode->getChildNode(next_letter) == NULL){ //game over, player stepped outside the graph
            this->in_progress = false; 
            printGameOverMsg(0,currentnode);
            return current_string;
    }
    currentnode = currentnode->getChildNode(next_letter); //move is legal, update currentnode
    if (currentnode->getIsLeaf()) { //did human make a word?
            this->in_progress = false; 
            printGameOverMsg(1,currentnode);
            return current_string;
    }
    char c = compMove(currentnode); //decide computer move
    currentnode = currentnode->getChildNode(c);
    if (currentnode->getIsLeaf()) { //did comp make a word?
            this->in_progress = false; 
            printGameOverMsg(2,currentnode);
            return current_string;
    }
    //send info back to client
    cout << "Game in progress.  The current word is: ";
    cout << currentnode->getString() << endl;

    return currentnode->getString();
}

char PlayGhost::compMove(Wordnode * currentnode) {
    vector<string> move_opts = currentnode->nextMoves();
    int i = rand()%move_opts.size(); //select randomly from move_opts
    return currentnode->nextChar(move_opts.at(i));	
}

void PlayGhost::printGameOverMsg(int condition, Wordnode * currentnode) {   					
    if (condition == 0) { //human player stepped outside the graph
        cout << "Looks like you tried to invent a new word..." << endl;
        cout << "I win!" << endl;
        cout << "It was possible to spell the word " << g->rndWordInBranch(currentnode) << endl;
    }
    else if (condition == 1) {//human player completed a word
        cout << "You completed the word " + currentnode->getString() << endl;
        cout << "I win!" << endl;
    }
    else { //comp player completed a word - human wins
        cout << "I was forced to complete the word " <<  currentnode->getString() << endl;
        cout << "YOU WIN!!!" << endl;

    }
}  
