/* 
 * Author: Aaron Shen
 *  
 * (c) 2015
 *    
*/


#include "Wordgraph.h"

#include <vector>
#include <string>
using namespace std;

Wordgraph::Wordgraph() {
     rootWordnode = new Wordnode("",false);
 }

 Wordgraph::~Wordgraph(){
     delete(rootWordnode);
 }

 void Wordgraph::tagAll () {
     rootWordnode->tagWinLose();
 }

 void Wordgraph::traverseWordGraph () {
     rootWordnode->walk();
 }

 Wordnode * Wordgraph::findWordnode(string s) {
     int len = s.length();
     Wordnode * currentnode = rootWordnode;      
     for (int i=0;i<len;i++) { //each loop iteration examines the i'th letter in the word
         Wordnode * next_node = currentnode->getChildNode(s.at(i));
         if (next_node!=NULL) {
             currentnode = next_node;
         }
         else {
             return NULL;
         }
     }
     return currentnode;
 }

 //returns a random word accessible from wordnode w
 string Wordgraph::rndWordInBranch(Wordnode * w) {
     Wordnode * currentnode = w;
     while(!currentnode->is_leaf) {             
         vector<Wordnode *> tmp = currentnode->getChildren();            
         currentnode = tmp[rand()%tmp.size()];
     }
     return currentnode->word_string;

 }

 //insInWordGraph: string -> bool
 //attempts to insert the string into the WordGraph.  If successful, return true.
 //if word has the same beginning as an existing word, return false and do not insert the word.
 //fails if:
 //1) s consists of (another word already in the graph + 0 or more letters)
 //2) s is too short
 bool Wordgraph::insInWordGraph (string s) {
     int len = s.length();
     if (len < min_length) { //check word length for minimum length
         //System.out.println("Skipped " + s + " (too short)"); 
         return false; //word is too short
     }

     //find correct node to insert the word in
     Wordnode * currentnode = rootWordnode;
     for (int i=0;i<len-1;i++) { //each loop iteration examines the i'th letter in string s, up to the second to last.
         if (currentnode->is_leaf) { //leaf node reached - this is a dupe branch
           return false;  //do not insert the word
         }
         Wordnode * next_node = currentnode->getChildNode(s.at(i));
         if (next_node != NULL) {
             currentnode = next_node;
         }
         else {
             Wordnode * treenode = new Wordnode(currentnode->word_string + s.at(i),false); //create a new intermediate WordNode
             currentnode->setChildNode(treenode,s.at(i));
             currentnode = treenode;
         }          
     } //loop terminates before last letter of word

     //currentNode is now the node that should contain our leaf node
     Wordnode * w = new Wordnode(s,true);  //create a leaf node
     if (currentnode->is_leaf) { //leaf node reached (i.e. inserting "apples" when "apple" is already present)  
         return false;  //leafs can't contain other leafs.  do not insert the word
     }
     if (currentnode->getChildNode(s.at(s.length()-1))){ //this should never actually happen
         return false;
     }
     currentnode->setChildNode(w,s.at(s.length()-1));

     return true;
 }
