/* 
 * File:   main.cpp
 * Author: Aaron Shen
 *
 * Created on November 4, 2015, 8:13 PM
 */

#include <cstdlib>
#include <ctime>
using namespace std;

#include "Wordgraph.h"
#include "PlayGhost.h"

/*
 * 
 */
int main(int argc, char** argv) {
    srand(time(NULL));
    PlayGhost * game = PlayGhost::initPlayGhost("words.txt");
    game->startGame();
    delete(game);

    return 0;
}

